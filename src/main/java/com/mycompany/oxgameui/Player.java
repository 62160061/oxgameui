/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NITRO 5
 */
package com.mycompany.oxgameui;

import java.io.Serializable;

public class Player implements Serializable{
    private char name;
  private  int win;
  private  int lose;
  private  int draw;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

   public void win(){
       win++;
   }public void lose(){
       lose++;
       
   }public void draw(){
       draw++;
   }

    public int getLose() {
        return lose;
    }
    public int getDraw() {
        return draw;
    }
    public void Win(){
        win++;
    }
    public void Lose(){
        lose++;
    }
    public void Draw(){
        draw++;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }
    
    public Player(char name){
    this.name = name;    
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
